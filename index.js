/* Server Express */
const express = require('express')
const app = express()
let cors = require('cors');
const port = 3000

/* Body parser */
const bodyParser = require('body-parser').json();

/* Properties for database */
var PropertiesReader = require('properties-reader');
var properties = PropertiesReader('config/db.properties')._properties;

/* Database */
const mysql = require('mysql2/promise');
const dbOpts = {
  host     : properties.host,
  user     : properties.username,
  password : properties.password,
  database : properties.database
};

async function executeQuery (strRequest) {
  const connection = await mysql.createConnection(dbOpts);
  return await connection.execute(strRequest);
}

/* Crypto */
const CryptoJS = require('crypto-js');

function sha256(content) {
  return CryptoJS.SHA256(content).toString(CryptoJS.enc.Base64);
}

app.use(cors());

app.get('/', (req, res) => res.send('Hello World!'));

app.get('/users', async (req,res) => {
  const [rows, fields] = await executeQuery('SELECT id,username FROM users;');
  res.status(200).contentType('application/json').send(rows);
});

app.get('/users/:id', async (req,res) => {
  const [rows, fields] = await executeQuery(`SELECT id,username FROM users where id=${req.params.id};`);
  res.status(200).contentType('application/json').send(rows);
});

app.post('/login', bodyParser, async (req, res) => {
  const [rows, fields] = await executeQuery(`SELECT id FROM users where username='${req.body.username}' AND passwrd='${sha256(req.body.password)}';`);
  if(rows.length === 0) {
    res.status(401).send("Credentials not matching !");
  } else {
    res.status(200).contentType('application/json').send(rows);
  }
});

app.listen(port, () => {console.log(`Example app listening on port ${port}!`);});